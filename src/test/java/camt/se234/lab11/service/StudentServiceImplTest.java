package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {

    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Before
    public void setup() {
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    @Test
    public void testFindById() {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("213"),is(new Student("213","B","emp",3.30)));
        assertThat(studentService.findStudentById("333"),is(new Student("333","D","tem",2.50)));

    }

    @Test
    public void testWithMock() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("213","B","emp",3.37));
        mockStudents.add(new Student("333","D","tem",2.70));
        mockStudents.add(new Student("331","F","te",2.60));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }

    @Test
    public void testGetAverageGpaParams(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(2.9459999999999997));
    }


    @Test
    public void testGetAverageGpa2Params(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(2.9459999999999997));
    }

    @Test
    public void testFindByPartOfId() {
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("213","B","emp",3.37));
        mockStudents.add(new Student("333","D","tem",2.70));
        mockStudents.add(new Student("331","F","te",2.60));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("33")
                ,hasItem(new Student("331","F","te",2.60)));
        assertThat(studentService.findStudentByPartOfId("33")
                ,hasItems(new Student("331","F","te",2.60)
                        ,new Student("333","D","tem",2.70)));

    }


    @Test
    public void testFindByPartOfId2() {
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("111","G","s",2.33));
        mockStudents.add(new Student("112","H","ss",3.30));
        mockStudents.add(new Student("113","I","sss",4.00));
        mockStudents.add(new Student("114","J","ssss",2.50));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("11")
                ,hasItems(new Student("111","G","s",2.33)
                        ,new Student("112","H","ss",3.30)
                        ,new Student("113","I","sss",4.00)));

    }


    @Test(expected = NoDataException.class)
    public void testNoDataException() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    @Test(expected = NoPartOfDataException.class)
    public void testNoPartOfDataException() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),nullValue());
    }

    @Test(expected = GpaDividedByZero.class)
    public void testGpaDividedByZero() {
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());

    }


}
