package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    List<Student> students2;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("213","B","emp",3.30));
        students.add(new Student("223","C","tep",4.00));
        students.add(new Student("333","D","tem",2.50));
        students.add(new Student("331","F","te",2.60));

        students2 = new ArrayList<>();
        students2.add(new Student("111","G","s",2.33));
        students2.add(new Student("112","H","ss",3.30));
        students2.add(new Student("113","I","sss",4.00));
        students2.add(new Student("114","J","ssss",2.50));
        students2.add(new Student("115","K","sssss",2.60));

    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }
}
